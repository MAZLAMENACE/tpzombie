import { Person, Variant } from "../person"
import { hasChildInfected, hasGrandchildInfected, isInfected } from "./conditions"
import { infectContactMoreThan32, infectContacts, infectContactsOneInTwo, infectPerson, } from "./effect"

export interface ConditionEffect {
  condition: (person: Person) => boolean,
  effect: (person: Person, variant?: Variant) => Person,
}

export const variantAState: ConditionEffect[] = [
  {
    condition: isInfected,
    effect: infectContacts,
  },
]
export const variantBState: ConditionEffect[] = [
  {
    condition: hasChildInfected,
    effect: infectPerson,
  },
  {
    condition: hasGrandchildInfected,
    effect: infectContacts,
  }
]

export const variant32State: ConditionEffect[] = [
  {
    condition: isInfected,
    effect: (person: Person, variant) => infectContactMoreThan32(person, variant),
  },
  {
    condition: hasChildInfected,
    effect: (person: Person, variant) => infectContactMoreThan32(person, variant),

  },
  {
    condition: hasGrandchildInfected,
    effect: (person: Person, variant) => infectContactMoreThan32(person, variant),
  }
]

export const variantCState: ConditionEffect[] = [
  {
    condition: hasChildInfected,
    effect: infectContactsOneInTwo,
  }
]