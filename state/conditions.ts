import { Person, Variant } from "../person";

export const isInfected = (person: Person): boolean => person.infected;
export const hasChildInfected = (person: Person): boolean => person.contacts.some(p => p.infected);
export const hasGrandchildInfected = (person: Person): boolean => {
  return person.contacts.some(
    subContacts => subContacts.contacts.some(p => p.infected)
  )
}
export const isMoreThan32Old = (person: Person): boolean => person.age > 32;
export const isYoung = (person: Person): boolean => person.age <= 30;
export const isInfectedByVariant = (person: Person, variant: Variant): boolean =>
  person.infected && person.variantType === variant
export const canInfectOthers = (person: Person): boolean => person.canInfectOther;
export const canChildrenInfectOthers = (person: Person): boolean => person.contacts.some(c => c.canInfectOther);
export const isImmunised = (person: Person): boolean => person.immunised;

export const hasInfected = (people: Person[]): boolean => {
  for (const person of people) {
    if (person.infected)
      return true;
    return hasInfected(person.contacts)
  }
  return false;
} 