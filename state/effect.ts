import { canChildrenInfectOthers, canInfectOthers, isImmunised } from "./conditions";
import { Person, Variant } from "../person";

export const infectContacts = (
  person: Person,
  variant?: Variant,
  childInfectionConditon: () => boolean = () => true
): Person => {
  if (canInfectOthers(person)) {
    person.contacts.map((p) => {
      if (!isImmunised(p) && childInfectionConditon()) {
        p.infected = true
        p.variantType = variant
        p.canInfectOther = true
      }
    });
  }
  return person;
}

export const infectContactsOneInTwo = (
  person: Person,
  variant?: Variant,
): Person => {
  if (canChildrenInfectOthers(person)) {
    person.contacts.map((p, i) => {
      if (!isImmunised(p)) {
        if (i % 2 === 0) {
          p.infected = true
          p.canInfectOther = true
          p.variantType = variant
        }
      }
    });
  }
  return person;
}

export const infectPerson = (
  person: Person,
  variant?: Variant,
): Person => {
  if (canChildrenInfectOthers(person) && !isImmunised(person)) {
    person.infected = true;
    person.canInfectOther = true;
    person.variantType = variant;
  }
  return person;
}

export const infectContactMoreThan32 = (
  person: Person,
  variant?: Variant,
): Person => {
  person.contacts.map(p => {
    if (!isImmunised(p) && p.age > 32) {
      p.infected = true
      p.variantType = variant
      p.canInfectOther = true
    }
  });
  return person;
}

export const immunise = (person: Person): Person => {
  person.infected = false;
  person.immunised = true;
  person.canInfectOther = false;
  person.variantType = undefined;
  return person;
}

export const killOrImmunise = (person: Person): Person => {
  const areYouLucky: number = Math.round(Math.random()); // 0 ou 1
  if (areYouLucky) {
    person.dead = true
    return person
  }
  person.infected = false;
  person.variantType = undefined;
  return person;
}

export const cannotInfectOther = (person: Person): Person => {
  person.canInfectOther = false;
  return person;
}