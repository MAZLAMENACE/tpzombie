export class Person {
  constructor(
    public name: string,
    public age: number,
    public infected: boolean = false,
    public variantType?: Variant
  ) { }
  public contacts: Person[] = [];
  public immunised: boolean = false;
  public dead: boolean = false;
  public canInfectOther: boolean = this.infected;

  addPeopleToContact(people: Person[]): void {
    this.contacts = [...this.contacts, ...people];
  }

  static groupSocial1 = (): Person[] => {
    const root: Person[] = [];
    const emily = new Person('Emily', 28);
    const ethan = new Person('Ethan', 27, true);

    ethan.addPeopleToContact([
      new Person('Manson', 28),
      new Person('Ava', 36),
    ])
    emily.addPeopleToContact([ethan,
      new Person('Sophia', 45),
      new Person('Titouan', 22)
    ])
    root.push(emily);
    return root;
  }

  static printPeople = (people: Person[], stage: number = -1) => {
    stage++;
    for (const person of people) {
      console.log(`${'|   '.repeat(stage)}|-- ${person.name} ${person.age} ans ${person.infected ? '🧟' : '👨'} ${person.variantType ? 'Variant ' + person.variantType : ''}${person.immunised ? 'immunisé' : ''}${person.dead ? '🪦' : ''} ${person.canInfectOther ? '🦠' : ''}`);
      if (person.contacts)
        Person.printPeople(person.contacts, stage)
    }
  }
}
export type Variant = 'A' | 'B' | '32' | 'C' | 'ultime';

