import { Person } from "../../person";
import { vaccinA1 } from "../../vaccin19";
import { variantA } from "../../virus19";

const group1: Person[] = Person.groupSocial1();

// VARIANT A
console.log('Première vague variant A');
Person.printPeople(group1)
variantA(group1);
console.log('Après première vague variant A');
Person.printPeople(group1)
console.log('Vaccination A1!');
vaccinA1(group1)
Person.printPeople(group1)
console.log('Deuxième vague variant A');
variantA(group1);
Person.printPeople(group1)
console.log("Manson n'est pas malade");

