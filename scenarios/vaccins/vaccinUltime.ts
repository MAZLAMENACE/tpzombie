import { Person } from "../../person";
import { vaccinUltime } from "../../vaccin19";
import { variantUltime } from "../../virus19";


const group1: Person[] = Person.groupSocial1();

// VARIANT A
console.log('Première vague variant ultime');
Person.printPeople(group1)
variantUltime(group1);
console.log('Après première vague variant ultime');
Person.printPeople(group1)
console.log('Vaccination Ultime!');
vaccinUltime(group1)
Person.printPeople(group1)
console.log('Deuxième vague variant Ultime');
variantUltime(group1);
Person.printPeople(group1)


