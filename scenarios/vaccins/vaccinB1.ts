import { Person } from "../../person";
import { vaccinB1 } from "../../vaccin19";
import { variantB } from "../../virus19";

const group1: Person[] = Person.groupSocial1();

// VARIANT A
console.log('Première vague variant B');
Person.printPeople(group1)
variantB(group1);
console.log('Après première vague variant B');
Person.printPeople(group1)
console.log('Vaccination B1!');
vaccinB1(group1)
Person.printPeople(group1)
console.log('Deuxième vague variant B');
variantB(group1);
Person.printPeople(group1)
console.log('Emily peut être vaccinée en ayant une chance de mourir');


