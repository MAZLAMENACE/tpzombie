import { Person } from "../../person";
import { variantB } from "../../virus19";


const group1: Person[] = Person.groupSocial1();

// VARIANT B
console.log('Before variant B');
Person.printPeople(group1)
variantB(group1);
console.log('After variant B');
Person.printPeople(group1)