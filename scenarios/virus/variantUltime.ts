import { Person } from "../../person";
import { variantUltime } from "../../virus19";

const group1: Person[] = Person.groupSocial1();

// VARIANT ULTIME
console.log('Before variant ultime');
Person.printPeople(group1)
variantUltime(group1);
console.log('After variant ultime');
Person.printPeople(group1)