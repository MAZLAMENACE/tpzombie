import { Person } from "../../person";
import { variantC } from "../../virus19";

const group1: Person[] = Person.groupSocial1();

// VARIANT C
console.log('Before variant C');
Person.printPeople(group1)
variantC(group1);
console.log('After variant C');
Person.printPeople(group1)