import { Person } from "../../person";
import { variant32 } from "../../virus19";

const group1: Person[] = Person.groupSocial1();

console.log('Before variant 32');
Person.printPeople(group1)
variant32(group1);
console.log('After variant 32');
Person.printPeople(group1)