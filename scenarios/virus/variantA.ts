import { Person } from "../../person";
import { variantA } from "../../virus19";


const group1: Person[] = Person.groupSocial1();

// VARIANT A
console.log('Before variant A');
Person.printPeople(group1)
variantA(group1);
console.log('After variant A');
Person.printPeople(group1)