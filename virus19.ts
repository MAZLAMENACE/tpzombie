import { hasInfected, isImmunised } from "./state/conditions";
import { Person, Variant } from "./person";
import { ConditionEffect, variant32State, variantAState, variantBState, variantCState } from "./state/states";

export const variantA = (people: Person[]) => infectDesc(people, variantAState, 'A');
export const variantB = (people: Person[]) => infectAcs(people, variantBState, 'B');
export const variant32 = (people: Person[]) => infectBoth(people, variant32State, '32');
export const variantC = (people: Person[]) => infectAcs(people, variantCState, 'C');
export const variantUltime = (people: Person[]) => infectFirst(people);


export const infectDesc = (
  people: Person[],
  states: ConditionEffect[],
  variant: Variant
): Person[] => {
  for (const person of people) {
    states.forEach(state => {
      if (state.condition(person))
        state.effect(person, variant)
    })
    if (person.contacts.length > 0)
      return infectDesc(person.contacts, states, variant)
  }
  return people;
}

export const infectAcs = (
  people: Person[],
  states: ConditionEffect[],
  variant: Variant
): Person[] => {
  for (const person of people) {
    if (person.contacts.length > 0)
      infectAcs(person.contacts, states, variant)
    states.forEach(state => {
      if (state.condition(person))
        state.effect(person, variant)
    })
  }
  return people;
}

const infectBoth = (people: Person[], states: ConditionEffect[], variant: Variant) => {
  return infectDesc(infectAcs(people, states, variant), states, variant)
}

export const infectFirst = (
  people: Person[],
  p?: Person,
): Person[] => {
  if (hasInfected(people)) {
    for (const person of people) {
      if (person.contacts.length > 0)
        infectFirst(person.contacts, person)
      if (p === undefined && !isImmunised(person)) {
        person.infected = true
        person.variantType = 'ultime'
        person.canInfectOther = true;
      }
    }
    return people;
  }
  return people
}


