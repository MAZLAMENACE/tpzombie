import { isInfectedByVariant, isYoung } from "./state/conditions";
import { cannotInfectOther, immunise, killOrImmunise } from "./state/effect";
import { Person } from "./person";
import { ConditionEffect } from "./state/states";

export const vaccinA1 = (people: Person[]) => vaccin(people, vaccinA1State);
export const vaccinB1 = (people: Person[]) => vaccin(people, vaccinA2State);
export const vaccinUltime = (people: Person[]) => vaccin(people, vaccinUltimeState);

const vaccinA1State: ConditionEffect = {
  condition: (person: Person) =>
    isYoung(person) &&
    isInfectedByVariant(person, 'A') ||
    isInfectedByVariant(person, '32'),
  effect: immunise
}

const vaccinA2State: ConditionEffect = {
  condition: (person: Person) =>
    isInfectedByVariant(person, 'B') ||
    isInfectedByVariant(person, 'C'),
  effect: killOrImmunise
}

const vaccinUltimeState: ConditionEffect = {
  condition: (person: Person) => isInfectedByVariant(person, 'ultime'),
  effect: (person: Person) => immunise(cannotInfectOther(person))
}

export const vaccin = (people: Person[], state: ConditionEffect): Person[] => {
  for (const person of people) {
    if (state.condition(person))
      state.effect(person)
    vaccin(person.contacts, state)
  }
  return people;
}
